package main

import (
	"math/rand"
)

type Frequency interface {
	Read() float64                                //Get latest value
	Trip(float64, float64, float64, bool) float64 //Generator trip
}

type GaussianWalker []float64 // smoothed out Grid frequency with N-period lookback
const WALKER_LOOKBACK = 8     // number of periods in lookback of Gaussian walker. Don't set this to 0!

func NewGaussianWalker() GaussianWalker {
	g := make(GaussianWalker, WALKER_LOOKBACK, WALKER_LOOKBACK)
	for i := 0; i < WALKER_LOOKBACK; i++ {
		g[i] = rand.NormFloat64()*0.1 + 50
	}
	return g
}

func (g GaussianWalker) Read() float64 {
	return rand.NormFloat64()*0.02 + 50
	//g = append(g[1:], rand.NormFloat64()*0.1+50)
	//var sum float64
	//for i := range g {
	//	sum += g[i]
	//}
	//return sum / WALKER_LOOKBACK
}

//Helper function to calculate Grid Frequency during a trip,
//while simulated response is being provided.
//If feedbackLoop is true, response/available will compensate a 0.5Hz drop, with
//linearity below that.
//If feedbackLoop is false, response/available will compensate a [magnitude]Hz drop,
//so that it will take 100% of participants to cancel out the trip.
func (g GaussianWalker) Trip(magnitude, available, response float64, feedbackLoop bool) float64 {
	next := g.Read()
	if available == 0 || magnitude == 0 {
		return next
	}
	next = next - magnitude //trip
	if feedbackLoop {
		return next + (response/available)*0.5
	} else {
		return next + (response/available)*magnitude
	}
}
