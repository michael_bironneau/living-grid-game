package main

import (
	"encoding/json"
	"math/rand"
	"sync/atomic"
)

type LoadPower int

const (
	SINGLE LoadPower = iota
	MULTIPLE
	CLIENT
)

type LoadType struct {
	Name      string  //Friendly name
	MinPower  float64 //Minimum power of this asset class
	MaxPower  float64 //Max power of that asset class
	Co2Factor float64 //Coefficient to multiply baseline (bitumen) CO2 savings by
}

type Load struct {
	Type      string  `json:"type"`
	Power     float64 `json:"power"`
	Co2Factor float64 `json:"co2_factor"`
	Off       bool    `json:ignore` // Load state
	Id        int64   `json:id`
}

var (
	LoadIdCounter = int64(0)
	LoadTypes     = []LoadType{
		LoadType{"Supermarket Chiller", 8.0, 12.0, 1.0},
		LoadType{"Bitumen tank", 35.0, 50.0, 1.0},
		LoadType{"Air handling unit", 7.0, 12.0, 1.0},
		LoadType{"Water pump", 80.0, 120.0, 1.0},
	}
	ClientTypes = []LoadType{
		LoadType{"Supermarket x 4", 500, 700, 1.0},
		LoadType{"Bitumen site x 10", 900, 1200, 1.0},
		LoadType{"Water treatment plant", 1200, 2000, 1.0},
	}
)

func init() {
	atomic.StoreInt64(&LoadIdCounter, 1)
}

//Generate a new load of randomly chosen type and autoincremented Id. Goroutine-safe.
func NewLoad(lp LoadPower) *Load {
	var lt LoadType
	switch lp {
	case SINGLE:
		lt = LoadTypes[rand.Intn(len(LoadTypes))]
	case MULTIPLE:
		lt = LoadTypes[rand.Intn(len(LoadTypes))]
		lt.Name += " x 10"
		lt.MinPower = lt.MinPower * 10
		lt.MaxPower = lt.MaxPower * 10
	case CLIENT:
		lt = ClientTypes[rand.Intn(len(LoadTypes))]
	}

	atomic.AddInt64(&LoadIdCounter, 1)
	return &Load{
		Type:      lt.Name,
		Power:     lt.MinPower + rand.Float64()*(lt.MaxPower-lt.MinPower),
		Co2Factor: lt.Co2Factor,
		Id:        atomic.LoadInt64(&LoadIdCounter),
	}
}

func (l *Load) JSON() ([]byte, error) {
	return json.Marshal(l)
}
