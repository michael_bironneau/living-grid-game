package main

import (
	"encoding/json"
	"errors"
	"sync"
	"time"
)

const (
	INITIAL_HISTORY_CAPACITY = 60
)

type PortfolioResponse struct {
	Timestamp     time.Time `json:"datetime"`
	Available     float64   `json:"available"`
	Response      float64   `json:"provided"`
	GridFrequency float64   `json:"frequency"`
	NumAvailable  int       `json:"num_available"`
	NumProvided   int       `json:"num_provided"`
}

type PortfolioResponseHistory struct {
	Items []*PortfolioResponse `json:"items"`
}

//Portfolio is a thread-safe FFR Portfolio simulator.
type Portfolio struct {
	sync.RWMutex
	loads           map[int64]*Load //map from load id to load
	History         *PortfolioResponseHistory
	FrequencyReader Frequency
	CurrentTrip     *Trip
}

type Trip struct {
	Magnitude    float64
	FeedbackLoop bool
}

type TripResponse struct {
	Tripped bool `json:"tripped"`
}

func NewPortfolio() *Portfolio {
	p := Portfolio{}
	p.History = &PortfolioResponseHistory{}
	p.History.Items = make([]*PortfolioResponse, 0, INITIAL_HISTORY_CAPACITY)
	p.loads = make(map[int64]*Load, 500)
	p.FrequencyReader = NewGaussianWalker()
	return &p
}

func (p *Portfolio) StartTrip(t *Trip) {
	p.Lock()
	p.CurrentTrip = t
	p.Unlock()
}

func (p *Portfolio) EndTrip() {
	p.Lock()
	p.CurrentTrip = nil
	p.Unlock()
}

func (p *Portfolio) Tripped() TripResponse {
	p.RLock()
	defer p.RUnlock()
	return TripResponse{
		Tripped: p.CurrentTrip != nil,
	}
}

func (p *Portfolio) Len() int {
	p.RLock()
	defer p.RUnlock()
	return len(p.loads)
}

//Add an available load to a portfolio
func (p *Portfolio) AddNewLoad(lp LoadPower) *Load {
	newLoad := NewLoad(lp)
	p.Lock()
	p.loads[newLoad.Id] = newLoad
	p.Unlock()
	return newLoad
}

//Return a copy of loads
func (p *Portfolio) Loads() []Load {
	p.RLock()
	defer p.RUnlock()
	ret := make([]Load, len(p.loads))
	counter := 0
	for _, load := range p.loads {
		ret[counter] = *load
		counter++
	}
	return ret
}

func (p *Portfolio) setState(id int64, state bool) error {
	p.Lock()
	defer p.Unlock()
	if load, ok := p.loads[id]; !ok {
		return errors.New("Load id not found in map")
	} else {
		load.Off = state
	}
	return nil
}

func (p *Portfolio) TurnOff(id int64) error {
	return p.setState(id, true)

}

func (p *Portfolio) TurnOn(id int64) error {
	return p.setState(id, false)
}

func (p *Portfolio) GetState(id int64) (bool, error) {
	p.RLock()
	defer p.RUnlock()
	if load, ok := p.loads[id]; ok {
		return load.Off, nil
	} else {
		return false, errors.New("load not found")
	}
}

func (p *Portfolio) RemoveLoadId(id int64) {
	p.Lock()
	defer p.Unlock()
	delete(p.loads, id)
}

func (p *Portfolio) Available() float64 {
	var sum float64
	for _, load := range p.loads {
		sum += load.Power
	}
	return sum
}

func (p *Portfolio) Provided() (float64, int) {
	var sum float64
	var count int
	for _, load := range p.loads {
		if load.Off {
			sum += load.Power
			count++
		}
	}
	return sum, count
}

//Sample availability and response and record the values in portfolio history
func (p *Portfolio) Sample() (available, provided, frequency float64, numAvailable, numProvided int) {
	p.RLock()
	defer p.RUnlock()
	available = p.Available()
	provided, numProvided = p.Provided()
	numAvailable = len(p.loads)
	if p.CurrentTrip != nil {
		frequency = p.FrequencyReader.Trip(p.CurrentTrip.Magnitude, available, provided, p.CurrentTrip.FeedbackLoop)
	} else {
		frequency = p.FrequencyReader.Read()
	}

	p.History.Items = append(p.History.Items, &PortfolioResponse{time.Now(), available, provided, frequency, numAvailable, numProvided})
	if len(p.History.Items) > INITIAL_HISTORY_CAPACITY {
		p.History.Items = p.History.Items[1:] //evict oldest item
	}
	return
}

func (p *Portfolio) JSON() ([]byte, error) {
	p.RLock()
	defer p.RUnlock()
	return json.Marshal(p.History.Items)
}

//Remove all items from portfolio history
func (p *Portfolio) ClearHistory() {
	p.Lock()
	p.History.Items = p.History.Items[:0]
	p.Unlock()
}
