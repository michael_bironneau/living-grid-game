package main

import (
	//"fmt"
	"sync"
	"testing"
)

func TestNew(t *testing.T) {
	p := NewPortfolio()
	p.AddNewLoad()
	if p.Len() != 1 {
		t.Errorf("Expected 1 load, instead got %v", p.Len())
	}
}

func TestRemove(t *testing.T) {
	p := NewPortfolio()
	p.AddNewLoad()
	l := p.AddNewLoad()
	p.AddNewLoad()
	p.RemoveLoadId(l.Id)
	if p.Len() != 2 {
		t.Errorf("Expected 2 loads, instead got %v", p.Len())
	}
	for id, _ := range p.Loads {
		if id == l.Id {
			t.Error("Expected my load Id to be gone, but it's still here")
		}
	}
}

func TestTurnOff(t *testing.T) {
	p := NewPortfolio()
	l1 := p.AddNewLoad()
	p.TurnOff(l1.Id)
	if state, err := p.GetState(l1.Id); err == nil && state != true {
		t.Error("Expected load to have turned off")
	} else if err != nil {
		t.Errorf("Got error %v\n", err)
	}
}

func TestStats(t *testing.T) {
	p := NewPortfolio()
	l1 := p.AddNewLoad()
	l2 := p.AddNewLoad()
	l3 := p.AddNewLoad()
	p.TurnOff(l2.Id)
	available := p.Available()
	if int(available) != int(l1.Power+l2.Power+l3.Power) {
		t.Errorf("Expected %v availability, got %v", l1.Power+l2.Power+l3.Power, available)
	}
	provided := p.Provided()
	if provided != l2.Power {
		t.Errorf("Expected %v provided, got %v", l2.Power, provided)
	}
}

func TestGoroutineCreation(t *testing.T) {
	s := sync.WaitGroup{}
	p := NewPortfolio()
	s.Add(3)
	go func() {
		p.AddNewLoad()
		l1 := p.AddNewLoad()
		p.RemoveLoadId(l1.Id)
		s.Done()
	}()
	go func() {
		p.AddNewLoad()
		s.Done()
	}()
	go func() {
		p.AddNewLoad()
		s.Done()
	}()
	s.Wait()
	if p.Len() != 3 {
		t.Errorf("expected 3 loads, got %v", p.Len())
	}
}
