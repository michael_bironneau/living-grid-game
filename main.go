package main

import (
	"encoding/json"
	"golang.org/x/net/websocket"
	"io"
	"log"
	"net/http"
	"time"
)

//Server. Routes:
// GET /portfolio - return list of loads
// WS /ffr - start participating. 1) Get new load; 2) Return load state 3) close when unavailable
// POST /trip/start - simulate a generator trip
// POST /trip/stop - stop tripping out the grid
// GET /tripped - return whether a generator is currently tripped or not
// GET /history - return aggregated response over time for last trip (?)
// GET /hide-stats - hide the stats window that shows up on the big screen at the end of each frequency event.
// GET / - serve participant index
// GET /grid - serve organizer index
// GET /overhead - serve overhead
var GlobalPortfolio *Portfolio

type WSProtocol struct {
	Action string `json:"action"`
	Value  int    `json:"value"`
}

func init() {
	GlobalPortfolio = NewPortfolio()
}

func addHeaders(w http.ResponseWriter, status int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
}

func writeJSON(w http.ResponseWriter, response interface{}) {
	if b, err := json.Marshal(response); err != nil {
		addHeaders(w, 500)
		return
	} else {
		addHeaders(w, 200)
		w.Write(b)
	}
}

func handleParticipantIndex(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "views/load.html")
}

func handleOrganizerIndex(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "views/grid.html")
}

func handleFrequencyIndex(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "views/frequency.html")
}

//Return list of loads
func handlePortfolio(w http.ResponseWriter, r *http.Request) {
	writeJSON(w, GlobalPortfolio.Loads())
}

//Websocket handler to start participating
func handleFFR(ws *websocket.Conn) {
	l := GlobalPortfolio.AddNewLoad(SINGLE)
	defer func() {
		GlobalPortfolio.RemoveLoadId(l.Id)
		ws.Close()
	}()

	//The protocol is as follows:
	//  - client initializes connection
	//  - server adds new SINGLE load and returns to client
	//  - client may
	//     request a new load with {"action": "new", "value": 0, 1, or 2} (the value is the LoadPower)
	//    or
	//     start response with {"action": "response", "value": 1}
	//    or
	//     stop response with {"action": "response", "value": 0}
	//
	//To terminate the session the client should close the connection.
	b, err := l.JSON()
	if err != nil {
		log.Print(err)
		return
	}
	ws.Write(b)

	for {
		var wsp WSProtocol
		err := websocket.JSON.Receive(ws, &wsp)
		if err == io.EOF {
			return
		} else if err != nil {
			log.Printf("error from ws client %v", err)
			return
		}
		switch wsp.Action {
		case "new":
			GlobalPortfolio.RemoveLoadId(l.Id)
			l = GlobalPortfolio.AddNewLoad(LoadPower(wsp.Value))
			b, err := l.JSON()
			if err != nil {
				log.Print(err)
				return
			}
			ws.Write(b)
		case "response":
			var err error
			if wsp.Value == 0 {
				err = GlobalPortfolio.TurnOn(l.Id)
			} else {
				err = GlobalPortfolio.TurnOff(l.Id)
			}
			if err != nil {
				log.Print(err)
			}

		}

	}
}

func handleTripped(w http.ResponseWriter, r *http.Request) {
	writeJSON(w, GlobalPortfolio.Tripped())
}

//Start a generator trip
func handleStartTrip(w http.ResponseWriter, r *http.Request) {
	GlobalPortfolio.StartTrip(&Trip{Magnitude: 0.3, FeedbackLoop: false})
	addHeaders(w, 200)
	w.Write([]byte("{\"message\": \"OK\"}"))
}

//End a generator trip
func handleEndTrip(w http.ResponseWriter, r *http.Request) {
	GlobalPortfolio.EndTrip()
	addHeaders(w, 200)
	w.Write([]byte("{\"message\": \"OK\"}"))
}

//Return portfolio history (available, provided, frequency since last trip reset)
func handleHistory(w http.ResponseWriter, r *http.Request) {
	if b, err := GlobalPortfolio.JSON(); err != nil {
		addHeaders(w, 500)
	} else {
		addHeaders(w, 200)
		w.Write(b)
	}
}

//hide stats window that shows up on big screen
func handleHideStats(w http.ResponseWriter, r *http.Request) {
	addHeaders(w, 400)
	w.Write([]byte("Not implemented"))
}

//Run the main simulation loop on another goroutine. This will exit when the main() goroutine exits.
func RunSimulationLoop() {
	go func() {
		for {
			<-time.After(time.Second)
			GlobalPortfolio.Sample()
		}
	}()
}

func main() {
	routes := map[string]func(http.ResponseWriter, *http.Request){
		"/":           handleParticipantIndex,
		"/portfolio":  handlePortfolio,
		"/trip/start": handleStartTrip,
		"/trip/end":   handleEndTrip,
		"/history":    handleHistory,
		"/hide-stats": handleHideStats,
		"/tripped":    handleTripped,
		"/overhead":   handleFrequencyIndex,
		"/leader":     handleOrganizerIndex,
	}
	for route, handler := range routes {
		http.Handle(route, http.HandlerFunc(handler))
	}
	http.Handle("/s/", http.StripPrefix("/s/", http.FileServer(http.Dir("static"))))
	http.Handle("/ffr", websocket.Handler(handleFFR))
	RunSimulationLoop()
	http.ListenAndServe(":8080", nil)
}
