"""
    Living Grid Game Automated Performance Test Suite
    ==============================================
    Author: Michael Bironneau <michael.bironneau@openenergi.com>

    Description: Uses Locust to test principal controllers and performance bottlenecks.
    This is not intended to be used in CI - rather, use the command

    locust --host=http://living-grid.cloudapp.net

    and then open http://localhost:8089 to view the HTTP interface.

    Note: Locust must be installed for Python 2.6+, doesn't support Python 3.
"""

from locust import HttpLocust, TaskSet, task
import websocket


class UserBehavior(TaskSet):
    def on_start(self):
        ws = websocket.WebSocket()
        ws.connect('ws://living-grid.cloudapp.net/ffr')

    @task(4)
    def index(self):
        self.client.get("/")

    @task(2)
    def tripped(self):
        self.client.get("/tripped")

    @task(2)
    def history(self):
        self.client.get("/history")

    @task(2)
    def portfolio(self):
        self.client.get("/portfolio")



class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 3000
    max_wait = 6000


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 3000
    max_wait = 6000
