//singleton that keeps track of game state and has game logic
var grid = {
    screen: 0
};

grid.startGame = function(){
    d3.select('#button').on('click', function(){
        grid.next();
    });
};

//advance to next screen
grid.next = function(){
    switch (grid.screen){
        case 0:
            //welcome/start event
            this._startTrip();
            break;
        case 1:
            //stop event
            this._endTrip();
            break;
        default:
            //hmmm...
            console.log('Erm...where am I?');
    }
};

grid._startTrip = function(){
    console.log('Event starting');
    grid.screen = 0;
    $.get('/trip/start', function(){
            d3.select('#button').attr('class', 'btn-after').select('p').text('End trip');
            grid.screen = 1;
    });
};

grid._endTrip = function(){
    console.log('Event ending');
    grid.screen = 1;
    $.get('/trip/end', function(){
        d3.select('#button').attr('class', 'btn-before').select('p').text('Start trip');
        grid.screen = 0;
    });
};
