//singleton that keeps track of game state and has game logic
var freq = {
    tripped: false,
    history: null,
    firstTime: true
};

freq.startGame = function(){

    window.setInterval(freq._update, 1000);
};

freq._update = function(){
	freq._updateTripped();
	freq._updateFrequency();
	freq._updateIcons();
    //make requests to server to update state
};

freq._render = function(){
    //update frequency chart after update
    this._updateFrequency();
    this._updateGeneration();
    this._updateDemand();
};

//update frequency chart
freq._updateFrequency = function(){
	$.get('/history', function(data){
		freq.history = {items: data};
		d3.select('#num-available').text(data[data.length-1].num_available);
		d3.select('#num-provided').text(data[data.length-1].num_provided);
		if (freq.history.items.length == 0) return;
		var chart = document.getElementById('chart');
		if (freq.firstTime){
			freqChart.create(chart, {width: chart.offsetWidth, height: chart.offsetHeight}, freq.history);
			freq.firstTime = false;
		} else {
			freqChart.update(chart, freq.history);
		}
	});
};

freq._updateTripped = function(){
	$.get('/tripped', function(data){
		freq.tripped = data.tripped;
		if (freq.tripped){
			d3.select('h1').text('Living Grid: What did we achieve together?');
		} else {
			d3.select('h1').text('Living Grid: What could we achieve together?');
		}
	});
};

//render the opacity of the generators according to:
// - whether we are in a trip (if not it's 100%)
// - how big the trip is (0.5 is full, anything less is partial)
freq._updateIcons = function(){
	if (freq.tripped){
		//show the "no generator" icon
		d3.select('#gen-img').attr('src', '/s/img/windfarm_off.svg').style('opacity', '0.15');
		//compute the opacity based on the latest availability
		var latest = freq.history.items[freq.history.items.length-1];
		var opacityScale = d3.scale.linear().domain([0, latest.available]).range([0.25, 1]);
		console.log(opacityScale(latest.provided));
		d3.select('#lg-logo').style('opacity', opacityScale(latest.provided));
		if (latest.frequency < 49.85){
			d3.select('#dem').classed('danger', true);
		} else {
			d3.select('#dem').classed('danger', false);
		}
		if (latest.frequency > 50.15){
			d3.select('#gen').classed('danger', true);
		} else {
			d3.select('#gen').classed('danger', false);
		}
	} else {
		d3.select('#gen-img').attr('src', '/s/img/windfarm_on.svg').style('opacity', '1');
		d3.select('#lg-logo').style('opacity', '0.25');
	}
};