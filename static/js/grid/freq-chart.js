var freqChart = {};

freqChart.create = function(el, props, state){
    var wrapper = d3.select(el).append('svg')
                    .attr('width', props.width)
                    .attr('height', props.height);
    d3.select(el).attr('data-width', props.width);
    d3.select(el).attr('data-height', props.height);


    this.update(el, state);


    var series = d3.select(el).select('path');
    var totalLength = series.node().getTotalLength();

       series
      .attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition()
        .duration(1000)
        .ease("linear")
        .attr("stroke-dashoffset", 0);
};

freqChart.update = function(el, state){
    var start = Date.parse(state.items[0].datetime);
    var finish = Date.parse(state.items[state.items.length - 1].datetime);
    var width = parseFloat(d3.select(el).attr('data-width'));
    var height = parseFloat(d3.select(el).attr('data-height'));
    var xScale = d3.scale.linear().domain([start, finish]).range([0 + width*0.01, 0.95*width]);
    var yScale = d3.scale.linear().domain([50.5, 49.5]).range([0, height]);
    var chart = d3.select(el).select('svg').html('');
     chart.append('line')
        .style("stroke-dasharray", ("3,3"))
        .attr("x1", 0)
        .attr("x2", width)
        .attr("y1", yScale(50))
        .attr("y2", yScale(50))
        .style("stroke", "black");
    var line = d3.svg.line()
        .interpolate("cardinal")
        .x(function(d){ return xScale(Date.parse(d.datetime)); })
        .y(function(d){ return yScale(d.frequency);});

    var series = chart.append("path")
        .attr("d", line(state.items))
        .attr("stroke", "white")
        .attr("stroke-width", "2")
        .attr("fill", "none");


    //add text
    chart.append("text")
        .attr("x", width*0.92)
        .attr("y", yScale(state.items[state.items.length-1].frequency))
        .attr("fill", "white")
        .style("font-size", "2vh")
        .text(state.items[state.items.length-1].frequency.toFixed(2) + 'Hz');

};

freqChart.mock = function(el, props){
    var items = [];
    var start = Date.parse('2015-11-01T12:00:00Z');
    //items.push({datetime: new Date(start - 1000).toISOString(), frequency: 49.5});
    for (var i=0; i<50; i++){
        items.push({
            datetime: new Date(start + i*1000).toISOString(),
            //frequency: 50
            frequency: Math.random()*0.1 - 0.05 + 50
        });
    }

    
    //items.push({datetime: new Date(start + 50*1000).toISOString(), frequency: 50.5});

    freqChart.create(el, props, {items: items});
};