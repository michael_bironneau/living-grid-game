//Response countdown
//Description: Radial chart showing remaining energy storage
//Props: width, height, innerRadius, barWidth
//State: value, max

var countdown = {};

countdown.create = function(el, props, state){
	var wrapper = d3.select(el).append('svg')
					.attr('width', props.width)
					.attr('height', props.height);
	d3.select(el).attr('data-width', props.width);
	d3.select(el).attr('data-height', props.height);
	d3.select(el).attr('data-ir', props.innerRadius);
	d3.select(el).attr('data-bw', props.barWidth);
	this.update(el, state);
};

countdown.update = function(el, state){
	var chart = d3.select(el).select('svg');
	var innerRadius = parseFloat(d3.select(el).attr('data-ir'));
	var barWidth = parseFloat(d3.select(el).attr('data-bw'));
	var offset = [parseFloat(d3.select(el).attr('data-width'))*0.5, 
	parseFloat(d3.select(el).attr('data-height'))*0.5]
	var horiz = 0;

	//create outer arc first
	var outer = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(innerRadius + barWidth)
        .startAngle(horiz)
        .endAngle(horiz + 2*Math.PI*(state.value/state.max));
    
    chart.append('g').append('svg:path').attr('d', outer).attr('class', 'outer-arc')
    .attr("transform", "translate(" + offset[0] + "," + offset[1] + "), scale(0.5), rotate(0)");

    var inner = d3.svg.arc()
        .innerRadius(innerRadius + barWidth*(0.5 - 0.2/1.61))
        //.innerRadius(0)
		.outerRadius(innerRadius + barWidth*(0.5 + 0.2/1.61))
        .startAngle(horiz)
        .endAngle(horiz + 2*Math.PI*(state.value/state.max));
		

        chart.append('g').append('svg:path').attr('d', inner).attr('class', 'outer-arc')
		.attr("transform", "translate(" + offset[0] + "," + offset[1] + "), scale(0.5), rotate(0)");
		
	var innerInner = d3.svg.arc()
		.innerRadius(0)
		.outerRadius(innerRadius + barWidth*(0.5 + 0.2/1.61))
		.startAngle(horiz)
		.endAngle(2*Math.PI);
	
	chart.append('g').append('svg:path').attr('d', innerInner).attr('class', 'inner-arc')
	.attr("transform", "translate(" + offset[0] + "," + offset[1] + "), scale(0.5), rotate(0)");
		
   
};
