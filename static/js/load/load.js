//singleton that keeps track of game state and has game logic
var load = {
    screen: 0,
    loadPower: 0,
    co2Coeff: 0.072,
    poundCoeff: 0.0055,
    kettleCoeff: 0.9/30, //it takes a kettle 30 seconds to boil (?)
    ws: null,
    ws_open : false
};

load.startGame = function(){
    this.next();
    d3.select('#menu-tweet').on('click', function(){

    });

    d3.select('#menu-restart').on('click', function(){
        load.screen = 0;
        load._clear();
        load._hideMenuButtons();
        load.next();
    });
};

load.onAssetReceived = function(asset){
    load._asset = load._formatAsset(asset);
};

load._formatAsset = function(asset){
    // this._asset = {
    //    name: "10 x Supermarket chiller",
    //    availability: 122,
    //    available: 10,
    //    remaining: 10
    //};
    return {
        name: asset.type,
        availability: asset.power,
        available: 30,
        remaining: 30
    };
};

//advance to next screen
load.next = function(){
    load.screen += 1;
    switch (load.screen){
        case 1:
            //welcome
            this._renderWelcome();
            this._showButton();
            break;
        case 2:
            //pre-response
            load._startWS();
            this._clear();
            load.onAssetReceived = function(asset){
                load._asset = load._formatAsset(asset);
                console.log("New asset", load._asset);
                this._updateResponseChart();
                this._makeCaption(load._asset.availability, load._asset.remaining);
                this._showButton();
            };
            break;
        case 3:
            //in-response
            this._startResponse();
            this._showButton();
            break;
        case 4:
            //post-response
            this._stopResponse();
            this._makePostCaption();
            this._showBackgroundCircle();
            this._showMenuButtons();
            break;
        case 5:
            //go back to start with higher load power
            this.screen = 0;
            this.next();
            break;
        default:
            //hmmm...
            console.log('Erm...where am I?');
    }
};

load._showMenuButtons = function(){
    d3.select('#menu-buttons').style('opacity', 1);
    d3.selectAll('#menu-buttons').classed('no-interaction', false);
};

load._hideMenuButtons = function(){
    d3.select('#menu-buttons').style('opacity', 0);
    d3.selectAll('#menu-buttons').classed('no-interaction', true);
};

load._startResponse = function(){
    if (!load._asset || !load.ws_open) return;
    load.ws.send(JSON.stringify({action: "response", value: 1}));
    d3.select('#button-wrapper').html('');
    load._refresh = window.setInterval(function(){
        load._asset.remaining -= 1;
        d3.select('#caption-legend').html('');
        load._makeCaption(0, load._asset.remaining);
        if (load._asset.remaining <= 0){
            d3.select('#chart').html('');
            window.clearInterval(load._refresh);
            load.next();
            return;
        }
        d3.select('#chart').html('');
        load._updateResponseChart();
    }, 1000);
};

load._stopResponse = function(){
    if (!load._asset || !load.ws_open) return;
    load.ws.send(JSON.stringify({action: "response", value: 0}));
    window.clearInterval(load._refresh);
    d3.select('#caption-title').html('');
    d3.select('#caption-legend').html('');
    d3.select('#button-wrapper').html('');
    load.ws.close();
    load.ws_open = false;
    load.ws = null;
};

load._clear = function() {
    d3.select('#caption-title').text('');
    d3.select('#caption-legend').html('');
    d3.select('#chart').html('');
    d3.select('#button-wrapper').html('');
};

load._renderWelcome = function(){
    d3.select('#caption-title').text('Welcome');
    var instructions = [
    "Add your assets to the Living Grid."
    ];
    for (var i=0; i<instructions.length; i++){
        d3.select('#caption-legend')
        .append('p').style('color', 'white').text(instructions[i]);
    }
};

load._updateResponseChart = function(){
        var responseChart = d3.select('#chart').node();
        var smallestDimension = responseChart.offsetWidth>responseChart.offsetHeight?responseChart.offsetHeight:responseChart.offsetWidth;
        countdown.create(responseChart, {
            width: responseChart.offsetWidth,
            height: responseChart.offsetHeight,
            innerRadius: smallestDimension*0.90,
            barWidth: 30
        }, {
            value: load._asset.remaining,
            max: load._asset.available
        });
};

load._showButton = function(){
    var container = d3.select('#button-wrapper');
    var btn = container.append('div').attr('id', 'button');
    switch (this.screen){
        case 1:
            btn.attr('class', 'btn-before')
                .html('<p style="top:40%">Go!</p>')
                .on('click', function(){
                    load.next();
            });
            break;
        case 2:
            btn.attr('class', 'btn-before')
                .html('<img src="/s/img/off-button.svg"/><p>OFF</p>')
                .on('click', function(){
                     load.next();
            });
            break;
        case 3:
        default:
            btn.attr('class', 'btn-after')
                .html('<img src="/s/img/off-button.svg"/><p>ON</p>')
                .on('click', function(){
                    load.next();
            });
    }
};

load._startWS = function(){
    load.ws = new WebSocket("ws://" + location.host + "/ffr");

    load.ws.onopen = function(){
        load.ws_open = true;
    };

    load.ws.onerror = function(error){
        load.ws_open = false;
        console.log(error);
        window.setTimeout(load._startWS, 1000); //try to reconnect once per second
    };

    load.ws.onmessage = function(event){
        load._asset = JSON.parse(event.data);
        load.onAssetReceived(load._asset);
    };
};

load._makeCaption = function(power, remaining){
    var template = '<<L>>';
    d3.select('#caption-title').text(template.replace('<<L>>', load._asset.name)).style("color", "rgb(63,63,62)");
    d3.select('#caption-legend')
        .append('p')
        .html('<strong>Power consumption</strong>: ' + power.toFixed(1) + 'Kw');
    d3.select('#caption-legend')
        .append('p')
        .html('<strong>Available off time</strong>: ' + remaining + 's');
};

load._makePostCaption = function(){
    var provided = (this._asset.available - this._asset.remaining)*this._asset.availability;
    var caption = d3.select('#caption-legend');
    var template = '<span class="red"><<L>></span>';
    d3.select('#caption-title').html(template.replace('<<L>>', load._asset.name));
    caption.html('');
    caption
        .append('p')
        .attr('class', 'red')
        .html('provided <strong><>Kw-secs</strong>'.replace('<>', provided.toFixed(0)));

    caption
        .append('p')
        .attr('class', 'red')
        .html('<strong><>mg</strong> of CO2 saved'.replace('<>', (provided*load.co2Coeff).toFixed(1)));

    caption
        .append('p')
        .attr('class', 'red')
        .html('<strong><>p</strong> saved for National Grid'.replace('<>', (provided*load.poundCoeff).toFixed(1)));

    caption
        .append('img')
        .attr('src', '/s/img/kettle.svg')
        .style('width', '15%')
        .style('max-width', '140px');

    caption
        .append('p')
        .attr('class', 'red')
        .html('equivalent to <strong><></strong> kettles boiled'.replace('<>', Math.ceil(provided*load.kettleCoeff)));
};

load._getAsset = function(loadPower){
    //TODO: actually code this
    if (!load.ws_open) return null;

    var msg = {
        "action": "new",
        "value": loadPower
    };

    load.ws.send(JSON.stringify(msg));

    // this._asset = {
    //    name: "10 x Supermarket chiller",
    //    availability: 122,
    //    available: 10,
    //    remaining: 10
    //};
};

load._showBackgroundCircle = function(){
    var responseChart = d3.select('#chart').node();
    var smallestDimension = responseChart.offsetWidth>responseChart.offsetHeight?responseChart.offsetHeight:responseChart.offsetWidth;
    d3.select('#chart').html('');
    var wrapper = d3.select('#chart').append('svg')
        .attr('width', responseChart.offsetWidth)
        .attr('height', responseChart.offsetHeight);
    wrapper.append('circle')
        .attr('cx', 0.5*responseChart.offsetWidth)
        .attr('cy', 0.5*responseChart.offsetHeight)
        .attr('r', 0.45*smallestDimension)
        .attr('fill', 'white');
};