# Living Grid Simulation Game

**Author**: Michael Bironneau <michael.bironneau@openenergi.com>

This repo contains the source code of Open Energi's Living Grid simulation game.

Use the command `go run bitbucket.org/michael_bironneau/living-grid -p 8080` and point a modern browser to http://localhost:8080.

N.B. By "modern browser" I mean IE11+, any evergreen browser or just about any mobile device.